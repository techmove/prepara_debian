#!/bin/bash
#-------------------------------------
# Criado por Luiz Paulo Pimenta
# Data criacao 20/07/2021 
# Ultima atualizacao dia 20/07/2021 
#-------------------------------------
#         Funcao
# AJUSTAR O SISTEMA OPERACIONAL 
#-------------------------------------

#Includes
source includes.sh



clear
echo -e "+------------------------------------------------------+"
echo -e "$BLUE  BEM VINDO AO ASSISTENTE DE PREPARACAO DO SERVIDOR$FECHA" 
echo -e "+------------------------------------------------------+"
echo -e "| $GREEN   AGORA SERAO INSTALADOS ALGUMAS ATUALIZACOES E PACOTES UTEIS$FECHA           |"
echo -e "+------------------------------------------------------+"
echo -e "|                   VERSAO 1.00"
echo -e "+------------------------------------------------------+"
echo -e " Criado por : $GREEN Luiz Paulo Pimenta $FECHA"
echo -e " Data       : $GREEN 20/07/2021$FECHA"
echo -e " Atualizacao: $GREEN 20/07/2021 $FECHA"
echo -e "+------------------------------------------------------+"
echo -e "$GREEN Instalacao iniciada pelo processo $FECHA $RED $$ $FECHA"
echo -e "+------------------------------------------------------+"
echo -e "$YELLOW Aguarde....$FECHA"
sleep 3

clear  #Limpa a tela


#Inicio
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#       Diretorios e arquivos utilizadas no script
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Data=$(date +'%d/%m/%Y')  #Verifica data atual
Hora=$(date +'%H:%M:%S')  #Verifica hora atual




echo -e "$GREEN ########################################################################################## $FECHA"
echo -e "$YELLOW   Script sendo executado com o processo $FECHA" "$RED $$ $FECHA" "$YELLOW no dia $Data as $Hora  $FECHA"
echo -e "$GREEN ########################################################################################## $FECHA"
echo " "
echo " "

#Verifica versao sistema
versao=$( cat /etc/*-release | grep -i pretty | cut -d'(' -f2 | sed 's/)"//g' )

#VERIFICA SE E UBUNTU
versaoUbuntu=$( cat /etc/*-release | grep -i pretty | cut -d'(' -f2 | sed 's/)"//g'  | cut -d'"' -f2 | cut -d" " -f 1 )

if [ "$versao" == "jessie" ] ; then

echo -e "$ORANGE Ajustando repositorio debian 8 $FECHA"
echo -e "$GREEN Atualizando arquivo de repositorios $FECHA"
cat arquivos/sources.list.debian8 > /etc/apt/source.list

elif [ "$versao" == "stretch" ]	; then

echo -e "$ORANGE Ajustando repositorio debian 9 $FECHA"
echo -e "$GREEN Atualizando arquivo de repositorios $FECHA"
cat arquivos/sources.list.debian9 > /etc/apt/source.list

elif [ "$versao" == "bullseye" ] ; then

echo -e "$ORANGE Ajustando repositorio debian 11 $FECHA"
echo -e "$GREEN Atualizando arquivo de repositorios $FECHA"
cat arquivos/sources.list.debian11 > /etc/apt/source.list

elif [ "$versao" == "bookworm" ] ; then

echo -e "$ORANGE Ajustando repositorio debian 12 $FECHA"
echo -e "$GREEN Atualizando arquivo de repositorios $FECHA"
cat arquivos/sources.list.debian12 > /etc/apt/source.list

elif [ "$versaoUbuntu" == "Ubuntu" ] ; then

echo -e "$ORANGE Instalação iniciada para UBUNTU $FECHA"

else

	echo "$RED !!!! ATENCAO !!!! ESTA VERSAO DO SISTEMA OPERACIONAL NAO E SUPORTADA PARA O SISTEMA TELMOVE. $FECHA"
fi	


echo " "


echo -e "$GREEN Atualizando lista de repositorios$FECHA"
apt-get update -y

echo -e "$GREEN Efetuando instalação APTITUDE $FECHA"
apt-get install aptitude -y


echo -e "$GREEN Atualizando o Sistema$FECHA"
aptitude upgrade -y


echo -e "$GREEN ########################################################################################## $FECHA"
echo -e "$YELLOW    		Iniciando a instalacao de alguns pacotes bacanas  $FECHA" 
echo -e "$GREEN ########################################################################################## $FECHA"
echo -e "$GREEN Instalando shell zsh$FECHA"
aptitude install zsh -y

echo -e "$GREEN Setando shell zsh como padrao$FECHA"
sed -i 's/root:\/bin\/bash/root:\/bin\/zsh/g' /etc/passwd

echo -e "$GREEN Ajustando cores do prompt $FECHA"
cp arquivos/.zshrc /root/

echo -e "$GREEN Instalando o VIM ( VI melhorado)$FECHA"
aptitude install vim -y

sleep 2
clear

echo -e "$GREEN Instalando servidor SSH$FECHA"
aptitude install openssh-server -y

echo -e "$GREEN SEta patch padrao dos aplicativos $FECHA"
echo PATH=\"/sbin:/bin:/usr/local/bin:/usr/sbin\" > /etc/environment

sleep 2
clear


echo -e "$GREEN INSTALANDO PACOTES UTEIS PARA O SISTEMA $FECHA"
apt-get update
apt-get install aptitude -y
aptitude install make -y
aptitude install gcc -y
aptitude install g++ -y
aptitude install libncurses5-dev -y
aptitude install uuid -y
aptitude install uuid-dev -y
aptitude install libjansson-dev -y
aptitude install libxml2-dev -y
aptitude install imagemagick -y
aptitude install nmap -y
aptitude install htop
aptitude install ethtool
aptitude install net-tools

echo -e "$RED !!!!!!!!!!!!!!!!!!!!!! PARABENS SEU SISTEMA FOI AJUSTADO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! $FECHA"

echo -e "$YELLOW
                ¶¶¶¶¶¶¶¶¶¶¶¶
                ¶¶            ¶¶
  ¶¶¶¶¶        ¶¶                ¶¶
  ¶     ¶     ¶¶      ¶¶    ¶¶     ¶¶
   ¶     ¶    ¶¶       ¶¶    ¶¶      ¶¶
    ¶    ¶   ¶¶        ¶¶    ¶¶      ¶¶
     ¶   ¶   ¶                         ¶¶
   ¶¶¶¶¶¶¶¶¶¶¶¶                         ¶¶
  ¶            ¶    ¶¶            ¶¶    ¶¶
 ¶¶            ¶    ¶¶            ¶¶    ¶¶
¶¶   ¶¶¶¶¶¶¶¶¶¶¶      ¶¶        ¶¶     ¶¶
¶               ¶       ¶¶¶¶¶¶¶       ¶¶
¶¶              ¶                    ¶¶
¶   ¶¶¶¶¶¶¶¶¶¶¶¶                   ¶¶
 ¶¶           ¶  ¶¶                ¶¶
 ¶¶¶¶¶¶¶¶¶¶¶¶    ¶¶            ¶¶
                 ¶¶¶¶¶¶¶¶¶¶¶ $FECHA"

sleep 3


